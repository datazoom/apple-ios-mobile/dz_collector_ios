// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6.1 (swiftlang-5.6.0.323.66 clang-1316.0.20.12)
// swift-module-flags: -target x86_64-apple-ios12.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DZ_Collector_iOS
import AVFoundation
import AVKit
import AdSupport
import AppTrackingTransparency
import Combine
import CoreTelephony
import Dispatch
import Foundation
import MediaPlayer
import Metal
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
public struct ChromecastMessage : DZ_Collector_iOS.DZNotification {
  public var sender: Any
  public var text: Swift.String {
    get
  }
  public static var name: Swift.String
  public init(sender: Any, text: Swift.String)
  public typealias Sender = Any
}
@_hasMissingDesignatedInitializers open class SwiftyBeaver {
  public static let version: Swift.String
  public static let build: Swift.Int
  public enum Level : Swift.Int {
    case verbose
    case debug
    case info
    case warning
    case error
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  public static var destinations: Swift.Set<DZ_Collector_iOS.BaseDestination> {
    get
  }
  @discardableResult
  open class func addDestination(_ destination: DZ_Collector_iOS.BaseDestination) -> Swift.Bool
  @discardableResult
  open class func removeDestination(_ destination: DZ_Collector_iOS.BaseDestination) -> Swift.Bool
  open class func removeAllDestinations()
  open class func countDestinations() -> Swift.Int
  open class func threadName() -> Swift.String
  open class func verbose(_ message: @autoclosure () -> Any, _ file: Swift.String = #file, _ function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  open class func debug(_ message: @autoclosure () -> Any, _ file: Swift.String = #file, _ function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  open class func info(_ message: @autoclosure () -> Any, _ file: Swift.String = #file, _ function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  open class func warning(_ message: @autoclosure () -> Any, _ file: Swift.String = #file, _ function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  open class func error(_ message: @autoclosure () -> Any, _ file: Swift.String = #file, _ function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  open class func custom(level: DZ_Collector_iOS.SwiftyBeaver.Level, message: @autoclosure () -> Any, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line, context: Any? = nil)
  public class func flush(secondTimeout: Swift.Int64) -> Swift.Bool
  @objc deinit
}
public protocol DZNotification {
  associatedtype Sender
  static var name: Swift.String { get }
  var sender: Self.Sender { get }
  func post()
  static func make<N>(from n: Foundation.Notification) -> N?
}
extension DZ_Collector_iOS.DZNotification {
  public static var notificationName: Foundation.Notification.Name {
    get
  }
  public func post()
  @discardableResult
  public static func on(on queue: Foundation.OperationQueue?, using block: @escaping (Self) -> Swift.Void) -> ObjectiveC.NSObjectProtocol
  @discardableResult
  public static func on(using block: @escaping (Self) -> Swift.Void) -> ObjectiveC.NSObjectProtocol
  public static func make<N>(from n: Foundation.Notification) -> N?
}
public protocol TypedNotificationCenter {
  func post<N>(_ notification: N) where N : DZ_Collector_iOS.DZNotification
  func addObserver<N>(_ forType: N.Type, sender: N.Sender?, queue: Foundation.OperationQueue?, using block: @escaping (N) -> Swift.Void) -> ObjectiveC.NSObjectProtocol where N : DZ_Collector_iOS.DZNotification
  @available(iOS 13.0, *)
  func publisher<N>(type: N.Type, sender: N.Sender?) -> Combine.AnyPublisher<N, Swift.Never> where N : DZ_Collector_iOS.DZNotification
}
extension Foundation.NotificationCenter : DZ_Collector_iOS.TypedNotificationCenter {
  public static var typedNotificationUserInfoKey: Swift.String
  public func post<N>(_ notification: N) where N : DZ_Collector_iOS.DZNotification
  public func addObserver<N>(_ forType: N.Type, sender: N.Sender?, queue: Foundation.OperationQueue?, using block: @escaping (N) -> Swift.Void) -> ObjectiveC.NSObjectProtocol where N : DZ_Collector_iOS.DZNotification
  @available(iOS 13.0, *)
  public func publisher<N>(type: N.Type, sender: N.Sender?) -> Combine.AnyPublisher<N, Swift.Never> where N : DZ_Collector_iOS.DZNotification
}
public protocol FilterType : AnyObject {
  func apply(_ value: Swift.String?) -> Swift.Bool
  func getTarget() -> DZ_Collector_iOS.Filter.TargetType
  func isRequired() -> Swift.Bool
  func isExcluded() -> Swift.Bool
  func reachedMinLevel(_ level: DZ_Collector_iOS.SwiftyBeaver.Level) -> Swift.Bool
}
@_hasMissingDesignatedInitializers public class Filters {
  public static let Path: DZ_Collector_iOS.PathFilterFactory.Type
  public static let Function: DZ_Collector_iOS.FunctionFilterFactory.Type
  public static let Message: DZ_Collector_iOS.MessageFilterFactory.Type
  @objc deinit
}
public class Filter {
  public enum TargetType {
    case Path(DZ_Collector_iOS.Filter.ComparisonType)
    case Function(DZ_Collector_iOS.Filter.ComparisonType)
    case Message(DZ_Collector_iOS.Filter.ComparisonType)
  }
  public enum ComparisonType {
    case StartsWith([Swift.String], Swift.Bool)
    case Contains([Swift.String], Swift.Bool)
    case Excludes([Swift.String], Swift.Bool)
    case EndsWith([Swift.String], Swift.Bool)
    case Equals([Swift.String], Swift.Bool)
    case Custom((Swift.String) -> Swift.Bool)
  }
  public init(_ target: DZ_Collector_iOS.Filter.TargetType, required: Swift.Bool, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level)
  public func getTarget() -> DZ_Collector_iOS.Filter.TargetType
  public func isRequired() -> Swift.Bool
  public func isExcluded() -> Swift.Bool
  public func reachedMinLevel(_ level: DZ_Collector_iOS.SwiftyBeaver.Level) -> Swift.Bool
  @objc deinit
}
@_inheritsConvenienceInitializers public class CompareFilter : DZ_Collector_iOS.Filter, DZ_Collector_iOS.FilterType {
  override public init(_ target: DZ_Collector_iOS.Filter.TargetType, required: Swift.Bool, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level)
  public func apply(_ value: Swift.String?) -> Swift.Bool
  override public func isExcluded() -> Swift.Bool
  @objc deinit
}
@_hasMissingDesignatedInitializers public class FunctionFilterFactory {
  public static func startsWith(_ prefixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func contains(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func excludes(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func endsWith(_ suffixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func equals(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func custom(required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose, filterPredicate: @escaping (Swift.String) -> Swift.Bool) -> DZ_Collector_iOS.FilterType
  @objc deinit
}
@_hasMissingDesignatedInitializers public class MessageFilterFactory {
  public static func startsWith(_ prefixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func contains(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func excludes(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func endsWith(_ suffixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func equals(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func custom(required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose, filterPredicate: @escaping (Swift.String) -> Swift.Bool) -> DZ_Collector_iOS.FilterType
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PathFilterFactory {
  public static func startsWith(_ prefixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func contains(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func excludes(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func endsWith(_ suffixes: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func equals(_ strings: Swift.String..., caseSensitive: Swift.Bool = false, required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose) -> DZ_Collector_iOS.FilterType
  public static func custom(required: Swift.Bool = false, minLevel: DZ_Collector_iOS.SwiftyBeaver.Level = .verbose, filterPredicate: @escaping (Swift.String) -> Swift.Bool) -> DZ_Collector_iOS.FilterType
  @objc deinit
}
extension DZ_Collector_iOS.Filter.TargetType : Swift.Equatable {
}
public func == (lhs: DZ_Collector_iOS.Filter.TargetType, rhs: DZ_Collector_iOS.Filter.TargetType) -> Swift.Bool
public class SBPlatformDestination : DZ_Collector_iOS.BaseDestination {
  public var appID: Swift.String
  public var appSecret: Swift.String
  public var encryptionKey: Swift.String
  public var analyticsUserName: Swift.String
  public var analyticsUUID: Swift.String {
    get
  }
  public struct SendingPoints {
    public var verbose: Swift.Int
    public var debug: Swift.Int
    public var info: Swift.Int
    public var warning: Swift.Int
    public var error: Swift.Int
    public var threshold: Swift.Int
  }
  public var sendingPoints: DZ_Collector_iOS.SBPlatformDestination.SendingPoints
  public var showNSLog: Swift.Bool
  public var serverURL: Foundation.URL?
  public var entriesFileURL: Foundation.URL
  public var sendingFileURL: Foundation.URL
  public var analyticsFileURL: Foundation.URL
  override public var defaultHashValue: Swift.Int {
    get
  }
  public init(appID: Swift.String, appSecret: Swift.String, encryptionKey: Swift.String, serverURL: Foundation.URL? = URL(string: "https://api.swiftybeaver.com/api/entries/"), entriesFileName: Swift.String = "sbplatform_entries.json", sendingfileName: Swift.String = "sbplatform_entries_sending.json", analyticsFileName: Swift.String = "sbplatform_analytics.json")
  override public func send(_ level: DZ_Collector_iOS.SwiftyBeaver.Level, msg: Swift.String, thread: Swift.String, file: Swift.String, function: Swift.String, line: Swift.Int, context: Any? = nil) -> Swift.String?
  public func sendNow()
  @objc deinit
}
open class BaseDestination : Swift.Hashable, Swift.Equatable {
  open var format: Swift.String
  open var asynchronously: Swift.Bool
  open var minLevel: DZ_Collector_iOS.SwiftyBeaver.Level
  open var levelString: DZ_Collector_iOS.BaseDestination.LevelString
  open var levelColor: DZ_Collector_iOS.BaseDestination.LevelColor
  open var calendar: Foundation.Calendar
  public struct LevelString {
    public var verbose: Swift.String
    public var debug: Swift.String
    public var info: Swift.String
    public var warning: Swift.String
    public var error: Swift.String
  }
  public struct LevelColor {
    public var verbose: Swift.String
    public var debug: Swift.String
    public var info: Swift.String
    public var warning: Swift.String
    public var error: Swift.String
  }
  public func hash(into hasher: inout Swift.Hasher)
  open var defaultHashValue: Swift.Int {
    get
  }
  public init()
  open func send(_ level: DZ_Collector_iOS.SwiftyBeaver.Level, msg: Swift.String, thread: Swift.String, file: Swift.String, function: Swift.String, line: Swift.Int, context: Any? = nil) -> Swift.String?
  public func execute(synchronously: Swift.Bool, block: @escaping () -> Swift.Void)
  public func executeSynchronously<T>(block: @escaping () throws -> T) rethrows -> T
  public func addFilter(_ filter: DZ_Collector_iOS.FilterType)
  public func removeFilter(_ filter: DZ_Collector_iOS.FilterType)
  @objc deinit
  open var hashValue: Swift.Int {
    get
  }
}
public func == (lhs: DZ_Collector_iOS.BaseDestination, rhs: DZ_Collector_iOS.BaseDestination) -> Swift.Bool
@objc public enum CustomMetadataStore : Swift.Int {
  case session, player
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public class Metadata : Swift.Sequence, Swift.IteratorProtocol {
  final public let type: DZ_Collector_iOS.CustomMetadataStore
  public var count: Swift.Int {
    get
  }
  public init()
  public init(type: DZ_Collector_iOS.CustomMetadataStore)
  public var description: Swift.String {
    get
  }
  public init(with type: DZ_Collector_iOS.CustomMetadataStore)
  public var keys: [Swift.String] {
    get
  }
  public func index(at index: Swift.Int) -> DZ_Collector_iOS.MetadataValue
  public func valueForKey(key: Swift.String) -> Any?
  public func delete(key: Swift.String) -> Any?
  public func value(at index: Swift.Int) -> DZ_Collector_iOS.MetadataValue
  public func set(value: DZ_Collector_iOS.MetadataValue, at index: Swift.Int)
  @discardableResult
  public func add(metadata: DZ_Collector_iOS.MetadataValue) -> Swift.Bool
  @discardableResult
  public func add(type: DZ_Collector_iOS.MetadataType, key: Swift.String, value: Any) -> Swift.Bool
  public func delete(at row: Swift.Int)
  public func clear()
  public func next() -> DZ_Collector_iOS.MetadataValue?
  public var exportValue: [[Swift.String : Any]] {
    get
  }
  public var jsonValue: Swift.String {
    get
  }
  public var objectValue: [Swift.String : Any] {
    get
  }
  public typealias Element = DZ_Collector_iOS.MetadataValue
  public typealias Iterator = DZ_Collector_iOS.Metadata
  @objc deinit
}
public struct MetadataValue {
  public let type: DZ_Collector_iOS.MetadataType
  public let key: Swift.String
  public let value: Any
  public init(int: Swift.Int, key: Swift.String, value: Any)
  public init(type: DZ_Collector_iOS.MetadataType, key: Swift.String, value: Any)
}
public enum MetadataType {
  case string, numeric, boolean
  public var string: Swift.String {
    get
  }
  public var intValue: Swift.Int {
    get
  }
  public static func == (a: DZ_Collector_iOS.MetadataType, b: DZ_Collector_iOS.MetadataType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct NotifyFramework : DZ_Collector_iOS.DZNotification {
  public var sender: Any
  public static var name: Swift.String
  @objc public enum Event : Swift.Int {
    case paused, resume
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  public init(sender: Any, event: DZ_Collector_iOS.NotifyFramework.Event)
  public typealias Sender = Any
}
@objc public protocol DZ_CollectorDelegate {
  @objc func play()
  @objc func pause()
  @objc func adsCompleted()
  @objc func adsStarted()
}
extension DZ_Collector_iOS.DZ_CollectorDelegate {
  public func play()
  public func pause()
  public func adsCompleted()
  public func adsStarted()
}
@objc public class DZ_Collector : ObjectiveC.NSObject {
  weak public var delegate: DZ_Collector_iOS.DZ_CollectorDelegate?
  public var customMetadata: DZ_Collector_iOS.DZCustomMetadata
  public var flagForAutomationOnly: Swift.Bool
  @objc public static func appStarted()
  @objc public init(configId: Swift.String, url: Swift.String)
  @objc public func start(completion: @escaping (Swift.Bool, Swift.Error?) -> ())
  @objc public func startCompleted()
  @objc public func deinitDatazoomSDK()
  @objc public func startPassiveRecording() -> Swift.String
  @objc(startRecordingEventsForPlayer:) public func startRecordingEvents(for playerInstance: AVFoundation.AVPlayer) -> Swift.String
  @objc public func set(playerName: Swift.String)
  @objc public func remove(player: AVFoundation.AVPlayer)
  @objc public func removeAllPlayers()
  @objc public func stopRecordingEvents()
  @objc(stopRecordingEventsForPlayer:) public func stopRecordingEvents(for playerInstance: AVFoundation.AVPlayer) -> Swift.String?
  @objc(stopRecordingEventsForPlayerId:) public func stopRecordingEvents(playerId id: Swift.String)
  @objc(updateBounds:) public func updateBounds(bounds: CoreGraphics.CGRect)
  @objc public var libVersion: Swift.String {
    @objc get
  }
  @objc(active) public var isActive: Swift.Bool {
    @objc get
  }
  @objc public var heartbeatInterval: Swift.Int {
    @objc get
  }
  @objc(customEvents:metadata:) public func customEvents(_ eventName: Swift.String?, metadata: [Swift.String : Any]? = nil)
  @objc public func customEvents(_ eventName: Swift.String?, playerContext: Swift.String, metadata: [Swift.String : Any]? = nil) -> Swift.Bool
  @objc(addCustomMetadata:) public func addCustom(metadata: [Swift.String : Any]?)
  @objc(customMetadata) public func getCustomMetadata() -> [Swift.String : Any]?
  @objc public func setCustom(metadata: [Swift.String : Any]?)
  @objc public func customMetadata(for playerContext: Swift.String) -> [Swift.String : Any]?
  @objc public func set(customMetadata metadata: [Swift.String : Any], playerContext: Swift.String) -> Swift.Bool
  @objc(addCustomEventName:metadata:) public func addCustom(event eventName: Swift.String?, withMetadata isMetadata: Swift.Bool)
  @objc(addChromecastEvent:metadata:) public func addChromecastEvent(_ event: [Swift.String : Any], metadata: [Swift.String : Any]? = nil)
  @objc public func chromecast(percent: Swift.Float)
  @objc(requestAdsForAdTagUrl:view:viewController:) public func requestAds(for adTagUrl: Swift.String, in view: UIKit.UIView, viewController: UIKit.UIViewController)
  @objc public func contentComplete()
  @objc(setContentPlayheadForPlayer:) public func setContentPlayhead(for player: AVFoundation.AVPlayer)
  @objc(addQueryStringToUrlString:sid:) public func addQueryString(to urlString: Swift.String, sid: Swift.String) -> Swift.String?
  @objc public func error(code: Swift.Int, message: Swift.String)
  @objc deinit
}
@available(iOS 13, *)
extension DZ_Collector_iOS.DZ_Collector {
  
  #if compiler(>=5.3) && $AsyncAwait
  @discardableResult
  public func start() async throws -> Swift.Bool
  #endif

}
@_inheritsConvenienceInitializers @objc public class DZCustomMetadata : ObjectiveC.NSObject {
  public var playerMetadata: DZ_Collector_iOS.Metadata
  public var sessionMetadata: DZ_Collector_iOS.Metadata
  public func clearAll()
  public var value: [Swift.String : Any] {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
extension Foundation.NSNotification.Name {
  public static var dzAdCompletedNotification: Foundation.Notification.Name {
    get
  }
}
public enum ConfigAPIError : Swift.Error {
  case loadingConfig
  case parsingConfig
  case connectorListEmpty
  case invalidUrl
  case networkDetailsError
  public var text: Swift.String {
    get
  }
  public static func == (a: DZ_Collector_iOS.ConfigAPIError, b: DZ_Collector_iOS.ConfigAPIError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum MilestonePoints {
  case p10
  case p25
  case p50
  case p75
  case p90
  case p95
  public static func == (a: DZ_Collector_iOS.MilestonePoints, b: DZ_Collector_iOS.MilestonePoints) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class Milestone {
  @objc deinit
}
public protocol TimersDelegate : AnyObject {
  func onHeartbeat()
  func onFastTimer()
}
open class Timers {
  weak public var delegate: DZ_Collector_iOS.TimersDelegate?
  @objc deinit
  public init(heartBeatInterval: Swift.Int, fastTimerInterval: Swift.Int)
  public func start()
  public func stop()
}
infix operator ?! : NilCoalescingPrecedence
public func ?! <A>(lhs: A?, rhs: Swift.Error) throws -> A
@_inheritsConvenienceInitializers public class ConsoleDestination : DZ_Collector_iOS.BaseDestination {
  public var useNSLog: Swift.Bool
  public var useTerminalColors: Swift.Bool {
    get
    set
  }
  override public var defaultHashValue: Swift.Int {
    get
  }
  override public init()
  override public func send(_ level: DZ_Collector_iOS.SwiftyBeaver.Level, msg: Swift.String, thread: Swift.String, file: Swift.String, function: Swift.String, line: Swift.Int, context: Any? = nil) -> Swift.String?
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class ChromecastState {
  @objc deinit
}
final public class GoogleCloudDestination : DZ_Collector_iOS.BaseDestination {
  public init(serviceName: Swift.String)
  override final public var asynchronously: Swift.Bool {
    get
    set
  }
  override final public func send(_ level: DZ_Collector_iOS.SwiftyBeaver.Level, msg: Swift.String, thread: Swift.String, file: Swift.String, function: Swift.String, line: Swift.Int, context: Any? = nil) -> Swift.String?
  @objc deinit
}
public struct AdRendition {
  public init(width: Swift.Int, height: Swift.Int, bitrate: Swift.Double)
}
open class FileDestination : DZ_Collector_iOS.BaseDestination {
  public var logFileURL: Foundation.URL?
  public var syncAfterEachWrite: Swift.Bool
  public var colored: Swift.Bool {
    get
    set
  }
  public var logFileMaxSize: (Swift.Int)
  public var logFileAmount: Swift.Int
  override public var defaultHashValue: Swift.Int {
    get
  }
  public init(logFileURL: Foundation.URL? = nil)
  override public func send(_ level: DZ_Collector_iOS.SwiftyBeaver.Level, msg: Swift.String, thread: Swift.String, file: Swift.String, function: Swift.String, line: Swift.Int, context: Any? = nil) -> Swift.String?
  public func deleteLogFile() -> Swift.Bool
  @objc deinit
}
extension DZ_Collector_iOS.SwiftyBeaver.Level : Swift.Equatable {}
extension DZ_Collector_iOS.SwiftyBeaver.Level : Swift.Hashable {}
extension DZ_Collector_iOS.SwiftyBeaver.Level : Swift.RawRepresentable {}
extension DZ_Collector_iOS.CustomMetadataStore : Swift.Equatable {}
extension DZ_Collector_iOS.CustomMetadataStore : Swift.Hashable {}
extension DZ_Collector_iOS.CustomMetadataStore : Swift.RawRepresentable {}
extension DZ_Collector_iOS.MetadataType : Swift.Equatable {}
extension DZ_Collector_iOS.MetadataType : Swift.Hashable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.Equatable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.Hashable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.RawRepresentable {}
extension DZ_Collector_iOS.ConfigAPIError : Swift.Equatable {}
extension DZ_Collector_iOS.ConfigAPIError : Swift.Hashable {}
extension DZ_Collector_iOS.MilestonePoints : Swift.Equatable {}
extension DZ_Collector_iOS.MilestonePoints : Swift.Hashable {}
