// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6.1 (swiftlang-5.6.0.323.66 clang-1316.0.20.12)
// swift-module-flags: -target arm64-apple-ios12.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DZ_Collector_iOS
import AVFoundation
import AVKit
import AdSupport
import AppTrackingTransparency
import Combine
import CoreMedia
import Dispatch
import Foundation
import GoogleInteractiveMediaAds
import MediaPlayer
import Metal
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
public enum DZError : Swift.Error {
  case wrongUrl
  case networkingError(Swift.Error)
  case serverError
  case requestError(Swift.Int, Swift.String)
  case invalidResponse
  case emptyData
  case notConnected
  case unknownError
  case configError(DZ_Collector_iOS.ConfigAPIError)
  public var errorMessage: Swift.String {
    get
  }
}
public struct ChromecastMessage : DZ_Collector_iOS.DZNotification {
  public var sender: Any
  public var text: Swift.String {
    get
  }
  public static var name: Swift.String
  public init(sender: Any, text: Swift.String)
  public typealias Sender = Any
}
public protocol DZNotification {
  associatedtype Sender
  static var name: Swift.String { get }
  var sender: Self.Sender { get }
  func post()
  static func make<N>(from n: Foundation.Notification) -> N?
}
extension DZ_Collector_iOS.DZNotification {
  public static var notificationName: Foundation.Notification.Name {
    get
  }
  public func post()
  @discardableResult
  public static func on(on queue: Foundation.OperationQueue?, using block: @escaping (Self) -> Swift.Void) -> ObjectiveC.NSObjectProtocol
  @discardableResult
  public static func on(using block: @escaping (Self) -> Swift.Void) -> ObjectiveC.NSObjectProtocol
  public static func make<N>(from n: Foundation.Notification) -> N?
}
public protocol TypedNotificationCenter {
  func post<N>(_ notification: N) where N : DZ_Collector_iOS.DZNotification
  func addObserver<N>(_ forType: N.Type, sender: N.Sender?, queue: Foundation.OperationQueue?, using block: @escaping (N) -> Swift.Void) -> ObjectiveC.NSObjectProtocol where N : DZ_Collector_iOS.DZNotification
  @available(iOS 13.0, *)
  func publisher<N>(type: N.Type, sender: N.Sender?) -> Combine.AnyPublisher<N, Swift.Never> where N : DZ_Collector_iOS.DZNotification
}
extension Foundation.NotificationCenter : DZ_Collector_iOS.TypedNotificationCenter {
  public static var typedNotificationUserInfoKey: Swift.String
  public func post<N>(_ notification: N) where N : DZ_Collector_iOS.DZNotification
  public func addObserver<N>(_ forType: N.Type, sender: N.Sender?, queue: Foundation.OperationQueue?, using block: @escaping (N) -> Swift.Void) -> ObjectiveC.NSObjectProtocol where N : DZ_Collector_iOS.DZNotification
  @available(iOS 13.0, *)
  public func publisher<N>(type: N.Type, sender: N.Sender?) -> Combine.AnyPublisher<N, Swift.Never> where N : DZ_Collector_iOS.DZNotification
}
@objc public enum CustomMetadataStore : Swift.Int {
  case session, player
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public class Metadata : Swift.Sequence, Swift.IteratorProtocol {
  final public let type: DZ_Collector_iOS.CustomMetadataStore
  public var count: Swift.Int {
    get
  }
  public init()
  public init(type: DZ_Collector_iOS.CustomMetadataStore)
  public var description: Swift.String {
    get
  }
  public init(with type: DZ_Collector_iOS.CustomMetadataStore)
  public var keys: [Swift.String] {
    get
  }
  public func index(at index: Swift.Int) -> DZ_Collector_iOS.MetadataValue
  public func valueForKey(key: Swift.String) -> Any?
  public func delete(key: Swift.String) -> Any?
  public func value(at index: Swift.Int) -> DZ_Collector_iOS.MetadataValue
  public func set(value: DZ_Collector_iOS.MetadataValue, at index: Swift.Int)
  @discardableResult
  public func add(metadata: DZ_Collector_iOS.MetadataValue) -> Swift.Bool
  @discardableResult
  public func add(type: DZ_Collector_iOS.MetadataType, key: Swift.String, value: Any) -> Swift.Bool
  public func delete(at row: Swift.Int)
  public func clear()
  public func next() -> DZ_Collector_iOS.MetadataValue?
  public var exportValue: [[Swift.String : Any]] {
    get
  }
  public var jsonValue: Swift.String {
    get
  }
  public var objectValue: [Swift.String : Any] {
    get
  }
  public typealias Element = DZ_Collector_iOS.MetadataValue
  public typealias Iterator = DZ_Collector_iOS.Metadata
  @objc deinit
}
public struct MetadataValue {
  public let type: DZ_Collector_iOS.MetadataType
  public let key: Swift.String
  public let value: Any
  public init(int: Swift.Int, key: Swift.String, value: Any)
  public init(type: DZ_Collector_iOS.MetadataType, key: Swift.String, value: Any)
}
public enum MetadataType {
  case string, numeric, boolean
  public var string: Swift.String {
    get
  }
  public var intValue: Swift.Int {
    get
  }
  public static func == (a: DZ_Collector_iOS.MetadataType, b: DZ_Collector_iOS.MetadataType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class DZCustomMetadata : ObjectiveC.NSObject {
  public var playerMetadata: DZ_Collector_iOS.Metadata
  public var sessionMetadata: DZ_Collector_iOS.Metadata
  public func clearAll()
  public var value: [Swift.String : Any] {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
public struct NotifyFramework : DZ_Collector_iOS.DZNotification {
  public var sender: Any
  public static var name: Swift.String
  @objc public enum Event : Swift.Int {
    case paused, resume
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  public init(sender: Any, event: DZ_Collector_iOS.NotifyFramework.Event)
  public typealias Sender = Any
}
@objc public protocol DZ_CollectorDelegate {
  @objc func play()
  @objc func pause()
  @objc func adsCompleted()
  @objc func adsStarted()
}
extension DZ_Collector_iOS.DZ_CollectorDelegate {
  public func play()
  public func pause()
  public func adsCompleted()
  public func adsStarted()
}
@objc public class DZ_Collector : ObjectiveC.NSObject {
  weak public var delegate: DZ_Collector_iOS.DZ_CollectorDelegate?
  public var flagForAutomationOnly: Swift.Bool
  @objc public static func appStarted()
  @objc public init(configId: Swift.String, url: Swift.String)
  @objc public func start(completion: @escaping (Swift.Bool, Swift.Error?) -> ())
  @objc public func deinitDatazoomSDK()
  @objc public func startPassiveRecording() -> Swift.String
  @objc(startRecordingEventsForPlayer:) public func startRecordingEvents(for playerInstance: AVFoundation.AVPlayer) -> Swift.String
  @objc public func set(playerName: Swift.String)
  @objc public func remove(player: AVFoundation.AVPlayer)
  @objc public func stopRecordingEvents()
  @objc(stopRecordingEventsForPlayer:) public func stopRecordingEvents(for playerInstance: AVFoundation.AVPlayer) -> Swift.String?
  @objc(stopRecordingEventsForPlayerId:) public func stopRecordingEvents(playerId id: Swift.String)
  @objc(updateBounds:) public func updateBounds(bounds: CoreGraphics.CGRect)
  @objc public var libVersion: Swift.String {
    @objc get
  }
  @objc(active) public var isActive: Swift.Bool {
    @objc get
  }
  @objc public var heartbeatInterval: Swift.Int {
    @objc get
  }
  @objc(customEvents:metadata:) public func customEvents(_ eventName: Swift.String?, metadata incoming: [Swift.String : Any]? = nil)
  @discardableResult
  @objc public func customEvents(_ eventName: Swift.String?, playerContext: Swift.String, metadata: [Swift.String : Any]? = nil) -> Swift.Bool
  @objc public func clearMetadata()
  @objc(clearMetadataInPlayerContext:) public func clearMetadata(playerContext: Swift.String)
  public func set(customMetadataObject customMetadata: DZ_Collector_iOS.Metadata)
  @objc(addCustomMetadata:) public func addCustom(metadata: [Swift.String : Any]?)
  @objc(customMetadata) public func getCustomMetadata() -> [Swift.String : Any]?
  @objc public func set(customMetadata metadata: [Swift.String : Any]?)
  @objc public func customMetadata(for playerContext: Swift.String) -> [Swift.String : Any]?
  public func getCustomMetadata(for playerContext: Swift.String) -> DZ_Collector_iOS.Metadata?
  public func setCustomMetadata(for playerContext: Swift.String, metadata: DZ_Collector_iOS.Metadata)
  @objc public func set(customMetadata metadata: [Swift.String : Any], playerContext: Swift.String) -> Swift.Bool
  @objc(addCustomMetadata:playerContext:) public func add(customMetadata metadata: [Swift.String : Any], playerContext: Swift.String) -> Swift.Bool
  @objc(addCustomEventName:metadata:) public func addCustom(event eventName: Swift.String?, withMetadata isMetadata: Swift.Bool)
  @objc(addChromecastEvent:metadata:) public func addChromecastEvent(_ event: [Swift.String : Any], metadata: [Swift.String : Any]? = nil)
  @objc public func chromecast(percent: Swift.Float)
  @objc(requestAdsForAdTagUrl:view:viewController:) public func requestAds(for adTagUrl: Swift.String, in view: UIKit.UIView, viewController: UIKit.UIViewController)
  @objc public func contentComplete()
  @objc(setContentPlayheadForPlayer:) public func setContentPlayhead(for player: AVFoundation.AVPlayer)
  @objc(addQueryStringToUrlString:sid:) public func addQueryString(to urlString: Swift.String, sid: Swift.String) -> Swift.String?
  @objc public func error(code: Swift.Int, message: Swift.String)
  @objc deinit
}
@available(iOS 13, *)
extension DZ_Collector_iOS.DZ_Collector {
  
  #if compiler(>=5.3) && $AsyncAwait
  @discardableResult
  public func start() async throws -> Swift.Bool
  #endif

}
extension Foundation.NSNotification.Name {
  public static var dzAdCompletedNotification: Foundation.Notification.Name {
    get
  }
}
public enum ConfigAPIError : Swift.Error {
  case loadingConfig
  case parsingConfig
  case connectorListEmpty
  case invalidUrl
  case networkDetailsError
  public var text: Swift.String {
    get
  }
  public static func == (a: DZ_Collector_iOS.ConfigAPIError, b: DZ_Collector_iOS.ConfigAPIError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct SessionExpired : DZ_Collector_iOS.DZNotification {
  public var sender: Any
  public var text: Swift.String? {
    get
  }
  public static var name: Swift.String
  public init(sender: Any, text: Swift.String?)
  public typealias Sender = Any
}
public enum MilestonePoints {
  case p10
  case p25
  case p50
  case p75
  case p90
  case p95
  public static func == (a: DZ_Collector_iOS.MilestonePoints, b: DZ_Collector_iOS.MilestonePoints) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
infix operator ?! : NilCoalescingPrecedence
public func ?! <A>(lhs: A?, rhs: Swift.Error) throws -> A
@_hasMissingDesignatedInitializers final public class ChromecastState {
  @objc deinit
}
postfix operator ++
public struct AdRendition {
  public init(width: Swift.Int, height: Swift.Int, bitrate: Swift.Double)
}
infix operator &= : DefaultPrecedence
extension DZ_Collector_iOS.CustomMetadataStore : Swift.Equatable {}
extension DZ_Collector_iOS.CustomMetadataStore : Swift.Hashable {}
extension DZ_Collector_iOS.CustomMetadataStore : Swift.RawRepresentable {}
extension DZ_Collector_iOS.MetadataType : Swift.Equatable {}
extension DZ_Collector_iOS.MetadataType : Swift.Hashable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.Equatable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.Hashable {}
extension DZ_Collector_iOS.NotifyFramework.Event : Swift.RawRepresentable {}
extension DZ_Collector_iOS.ConfigAPIError : Swift.Equatable {}
extension DZ_Collector_iOS.ConfigAPIError : Swift.Hashable {}
extension DZ_Collector_iOS.MilestonePoints : Swift.Equatable {}
extension DZ_Collector_iOS.MilestonePoints : Swift.Hashable {}
